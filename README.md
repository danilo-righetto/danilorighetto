# SUMMARY

- [PROJECT INTRODUCTION](#project-introduction)
- [MAIN PROJECT TECHNOLOGIES](#main-project-technologies)
- [MAIN LIBRARIES](#main-libraries)
- [ORGANIZATION OF THE PROJECT DIRECTORY STRUCTURE](#organization-of-the-project-directory-structure)
- [SETUP LOCAL](#setup-local)
- [ILLUSTRATIONS](#illustrations)
- [USING EMOJIS](#using-emojis)
- [LINKING PORTFOLIO TO GITHUB](#linking-portfolio-to-github)
- [HOW TO RUN LOCAL TEST](#how-to-run-local-test)
- [DEPLOY](#deploy)
- [APPLICATION ENVIRONMENTS LINKS](#application-environments-links)
- [ENVIRONMENT VARIABLES](#environment-variables)
- [BUSINESS RULES AND REQUIREMENTS](#business-rules-and-requirements)
- [CACHE RULES](#cache-rules)
- [TROUBLESHOOTING](#troubleshooting)
- [CHANGELOG](#changelog)
- [USEFUL REFERENCES AND LINKS](#useful-references-and-links)


# PROJECT INTRODUCTION

The current project has only one main objective::

- Be the portfolio of Danilo Righetto's work;

## WHAT IS THE FINAL GOAL OF THE PRODUCT?

Present all the information about Danilo Righetto's career.

   
## WHO ARE THE END USERS?

All recruiters and people interested in the services provided by Danilo Righetto.


# MAIN PROJECT TECHNOLOGIES

-   [React](https://reactjs.org/)
-   [axios](https://www.npmjs.com/package/axios)
-   [reactstrap](https://reactstrap.github.io/)
-   [react-reveal](https://www.react-reveal.com/)
-   [react-lottie](https://www.npmjs.com/package/react-lottie)
-   [react-easy-emoji](https://github.com/appfigures/react-easy-emoji)
-   [react-headroom](https://github.com/KyleAMathews/react-headroom)
-   [color-thief](https://github.com/lokesh/color-thief)


# MAIN LIBRARIES

- [apollo/client 3.3.21](https://www.npmjs.com/package/apollo-client/v/3.3.21)

- [axios 0.21.1](https://www.npmjs.com/package/axios/v/0.21.1)

- [react 17.0.2](https://www.npmjs.com/package/react/v/17.0.2)

- [colorthief 2.3.2](https://www.npmjs.com/package/colorthief/v/2.3.2)


# ORGANIZATION OF THE PROJECT DIRECTORY STRUCTURE

```
danilorighetto
.
├── jsconfig.json
├── LICENSE
├── package.json
├── picture.PNG
├── public
│   ├── favicon.png
│   ├── images
│   ├── index.html
│   └── manifest.json
├── README.md
└── src
    ├── App.jsx
    ├── assets
    │   ├── css
    │   │   ├── argon-design-system-react.css
    │   │   ├── argon-design-system-react.css.map
    │   │   └── argon-design-system-react.min.css
    │   ├── fonts
    │   │   └── Agustina.otf
    │   ├── img
    │   │   └── icons
    │   │       └── common
    │   │           ├── airbnbLogo.png
    │   │           ├── github.svg
    │   │           └── google.svg
    │   ├── lottie
    │   │   ├── build.json
    │   │   ├── coding.json
    │   │   └── webdev.json
    │   └── vendor
    │       ├── font-awesome
    │       │   ├── css
    │       │   │   ├── font-awesome.css
    │       │   │   └── font-awesome.min.css
    │       │   └── fonts
    │       │       ├── FontAwesome.otf
    │       │       ├── fontawesome-webfont.eot
    │       │       ├── fontawesome-webfont.svg
    │       │       ├── fontawesome-webfont.ttf
    │       │       ├── fontawesome-webfont.woff
    │       │       └── fontawesome-webfont.woff2
    │       └── nucleo
    │           ├── css
    │           │   ├── nucleo.css
    │           │   └── nucleo-svg.css
    │           └── fonts
    │               ├── nucleo-icons.eot
    │               ├── nucleo-icons.svg
    │               ├── nucleo-icons.ttf
    │               ├── nucleo-icons.woff
    │               └── nucleo-icons.woff2
    ├── components
    │   ├── DisplayLottie.jsx
    │   ├── EdutionCard.jsx
    │   ├── ExperienceCard.jsx
    │   ├── FeedbackCard.jsx
    │   ├── GithubProfileCard.jsx
    │   ├── Loading.jsx
    │   ├── Navigation.css
    │   ├── Navigation.jsx
    │   ├── ProjectsCard.jsx
    │   └── SocialLinks.jsx
    ├── containers
    │   ├── Education.jsx
    │   ├── Experience.jsx
    │   ├── Feedbacks.jsx
    │   ├── GithubProfile.jsx
    │   ├── Greetings.jsx
    │   ├── Proficiency.jsx
    │   ├── Projects.jsx
    │   └── Skills.jsx
    ├── index.js
    └── portfolio.js
```

<details>
  <summary>public</summary>

`Contains the project's public information.`
</details>

<details>
  <summary>src</summary>

    `Directory containing the customizable code.`

  <details>
    <summary>src > assets</summary>

    `Directory where layout files are available`

  </details>

  <details>
    <summary>src > components</summary>

    `Directory where component files are available for use in the project.`
  </details>

  <details>
    <summary>src > containers</summary>

    `Directory where page files are available.`
  </details>

</details>

# SETUP LOCAL

**First of all, make sure you are at the root of the project..**

```bash
# Clone this repository

# Go into the repository
$ cd danilorighetto

# Install dependencies
$ yarn

# Start's development server
$ yarn start
```

# ILLUSTRATIONS

- [Lottie File Source](https://lottiefiles.com)

# LINKING PORTFOLIO TO GITHUB

```javascript
  //  portfolio.js
  githubUserName: 'YOUR GITHUB USERNAME HERE',
```

# USING EMOJIS

For adding emoji 😃 into the texts in `Portfolio.js`, use the `emoji()` function and pass the text you need as an argument. This would help in keeping emojis compatible across different browsers and platforms.

# HOW TO RUN LOCAL TEST

N/A

## CODE COVERAGE

N/A

# DEPLOY

N/A

## PIPES

N/A

# APPLICATION ENVIRONMENTS LINKS

N/A

# ENVIRONMENT VARIABLES

N/A

# BUSINESS RULES AND REQUIREMENTS

N\A

# CACHE RULES

N\A

# TROUBLESHOOTING

Workaround solutions

N\A

# CHANGELOG

The Changelog with the change events is available in the [CHANGELOG.md](./CHANGELOG.md) file at the root of the project.

# USEFUL REFERENCES AND LINKS

- [Sentry Doc](https://docs.sentry.io/platforms/dotnet/aspnetcore/)

- [Test Pattern AAA](https://medium.com/@pjbgf/title-testing-code-ocd-and-the-aaa-pattern-df453975ab80)

- [Semantic Versioning](https://semver.org/)

- [Keep a CHANGELOG](https://keepachangelog.com/en/1.0.0/)
